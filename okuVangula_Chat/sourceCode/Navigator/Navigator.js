import React, { Component } from 'react';
import { createStackNavigator } from 'react-navigation-stack';
import { createAppContainer } from 'react-navigation';
import LoginScreen from '../Screens/mainScreens/LoginScreen';
import SignScreen from '../Screens/mainScreens/SignScreen';
import SplashScreen from '../Screens/SplashScreen';

//This naviagtor shows first the Splash Screen and after Login Screen
const Login_OR_SignUP_stackNavigator = createStackNavigator({
    
    Login: LoginScreen,
    SignUp: SignScreen,
    Splash: SplashScreen,
},
    {
        initialRouteName:'Splash',
        headerMode: 'none',
        navigationOptions: {
            headerVisible: false,
        }
    });

export default createAppContainer(Login_OR_SignUP_stackNavigator);




