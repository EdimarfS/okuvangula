import React, { Component } from 'react';
import firebase from 'firebase';
import { Text, View, TextInput, TouchableOpacity } from 'react-native';
import { Inputs, Button } from './index';




class LoginForm extends Component {

    render() {
        const { container, Input } = style;
        return (
            <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
                <Inputs
                    placeholder="email"
                    autoCapitalize='none'
                    autoCorrect={false}
                    returnKeyType='next'
                    placeholderTextColor='black'
                />
                <Inputs
                    placeholder="password"
                    autoCapitalize='none'
                    autoCorrect={false}
                    returnKeyType='go'
                    placeholderTextColor='black'
                    secureTextEntry

                />

                <Button
                    title="Login" />
            </View>

        );
    }









}

const style = {
    container: {
        flex: 1,

    }

}



export default LoginForm;