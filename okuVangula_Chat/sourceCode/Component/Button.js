import React, { Component } from 'react';
import { StyleSheet, Text, View, TouchableOpacity, ShadowPropTypesIOS } from 'react-native';
import Color from './Color';



//Buttons are defined here - In case I need to use multiple times
//I defined as reuseble here - In the future I will use redx and it will be changed

const Button = ({ title, onPress }) => {

    const { container, LoginButton } = style;

    return (
        <View style={container}>
            <TouchableOpacity
                style={{backgroundColor:'white'}}
                onPress={onPress}
            >
                <Text
                    style={LoginButton}>
                    {title}
                </Text>

            </TouchableOpacity>
        </View>
    );


}

const style = {
    container: {
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
        marginTop:10,

    },

    LoginButton: {
        textAlign: 'center',
        color: 'black',
        textAlign: 'center',
        fontSize: 24,
        height: 50,
        width: 350,
        fontSize: 18,
        padding: 10,
        
    },
};


export { Button };