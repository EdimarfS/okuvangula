import React, { Component } from 'react';
import { StyleSheet, Text, View, TouchableOpacity, TextInput } from 'react-native';
import Color from './Color';


//Inputs are defined here - In case I need to use multiple times
//I defined as reuseble here - In the future I will use redx and it will be changed

const Inputs = ({styles, placeholder, placeholderTextColor, value, onChangeText, returnKeyType, autoCapitalize,autoCorrect,secureTextEntry}) => {

    const { container, Input} = style;

    return (

        <View style={container}>
            <TextInput
                style={Input}
                placeholder={placeholder}
                placeholderTextColor={placeholderTextColor}
                autoCapitalize={autoCapitalize}
                autoCorrect={autoCorrect}
                value={value}
                onChangeText={onChangeText}
                returnKeyType={returnKeyType}
                secureTextEntry={secureTextEntry}
            />

        </View>
    );


}

const style ={
    container: {
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
        marginBottom: 10,
    },

    Input: {
        color: Color.white,
        backgroundColor:Color.purple_Dark,
        height: 50,
        width: 350,
        fontSize: 18,
        padding: 10,
       // borderRadius: 30,


    },
};


export  {Inputs};