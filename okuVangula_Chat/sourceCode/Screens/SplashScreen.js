import React, { Component } from 'react';
import { Text, View,SafeAreaView } from 'react-native';
import LottieView from 'lottie-react-native';
import boxSpinner from '../Component/lottieAnimation/boxSpinner.json';
import chat from '../Component/lottieAnimation/chat.json';
import Color from '../Component/Color';

//The structure of the program
//This is the Splash Screen, it will be shown before any other screen,there will be included the app Log and/or Company Name



//LottieView - is a Lib that I used to give on my app some animations
//Splash Screen animations
class SplashScreen extends Component {

    //HERE I used the setTimeout to give 2000 seconds to the splash screen
    //This function is useful to implement Splash Screen
    UNSAFE_componentWillMount() {
        setTimeout(() => {
            this.props.navigation.navigate('Login');

        }, 2000);
    }

    render() {
        const { container,lottieStyle } = style;
        return (
            <View style={container}>
                
              <LottieView
                style={lottieStyle}
                source={chat}
                autoPlay
                loop
                />
                <View>
                    
                    </View>
                
            </View>
        );
    }

}

const style = {
    container: {
        flex: 1,
        backgroundColor: Color.purple,
        alignItems: 'center',
        justifyContent: 'center',
    },

    lottieStyle:{
        height: 400,
        width: 400,

    }
};


export default SplashScreen;