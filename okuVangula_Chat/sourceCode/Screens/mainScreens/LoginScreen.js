import React, { Component } from 'react';
import { StyleSheet, Text, View,KeyboardAvoidingView } from 'react-native';
import LoginForm from '../../Component/LoginForm';
import {LinearGradient} from 'expo-linear-gradient';
import Color from '../../Component/Color';


//The future LoginScreen
class LoginScreen extends Component {

  render() {
    const { container } = style;
    return (
      <KeyboardAvoidingView behavior="padding" style={container} >
        <LoginForm/>
      </KeyboardAvoidingView>
    );
  }

}

const style = {
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor:Color.purple,
  },
};


export default LoginScreen;